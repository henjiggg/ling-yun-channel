﻿using ChannelApp.Models; 
using ChannelDemo.Services;
using Impinj.OctaneSdk;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace ChannelApp.Services
{
    /// <summary>
    /// 读写器服务
    /// </summary>
    public class Reader420Service : BindableBase, IReader
    {
        public Reader420Service()
        {
            Tags = new ObservableCollection<TagInfo>();
        }

        private ObservableCollection<TagInfo> tags;

        public ObservableCollection<TagInfo> Tags
        {
            get { return tags; }
            set { tags = value; }
        }

        private ImpinjReader reader;

        public bool IsOpen => reader.IsConnected;

        public void Connect()
        {
            if (IsOpen) return;

            reader.Connect("192.168.1.100");
            reader.TagsReported += Reader_TagsReported;
            Impinj.OctaneSdk.Settings settings = reader.QueryDefaultSettings();
            settings.SearchMode = SearchMode.SingleTarget;
            settings.ReaderMode = ReaderMode.AutoSetDenseReader;
            settings.Session = 1;
            settings.Antennas.GetAntenna(1).IsEnabled = true;
            settings.Antennas.GetAntenna(2).IsEnabled = true;
            settings.Antennas.GetAntenna(3).IsEnabled = true;
            settings.Antennas.GetAntenna(4).IsEnabled = true;
            settings.Antennas.GetAntenna(1).TxPowerInDbm = 32.5;
            settings.Antennas.GetAntenna(2).TxPowerInDbm = 32.5;
            settings.Antennas.GetAntenna(3).TxPowerInDbm = 32.5;
            settings.Antennas.GetAntenna(4).TxPowerInDbm = 32.5;
            reader.ApplySettings(settings);
        }

        public void Start(bool IsReset = true)
        {
            if (IsOpen) return;

            if (IsReset) CacheReset();

            reader.Start();
        }

        public void Stop()
        {
            if (!IsOpen) return;

            reader.Stop();
        }

        public void Close()
        {
            if (!IsOpen) return;

            reader.Disconnect();
        }

        public void CacheReset()
        {
            Tags.Clear();
        }

        /// <summary>
        /// 读写器回调事件
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="report"></param>
        private void Reader_TagsReported(ImpinjReader reader, TagReport report)
        {
            foreach (var tag in report.Tags)
            {
                var epc = tag.Epc.ToString().Replace(" ", "");
                if (Tags.FirstOrDefault(t => t.EPC == epc) == null)
                {
                    Tags.Add(new TagInfo()
                    {
                        EPC = epc,
                        TID = tag.Tid.ToString(),
                        Date = DateTime.Now,
                    });
                }
            }
        }
          
        public void SetParameter(object parameter)
        {
            //settings.Session = 1;
            //settings.Antennas.GetAntenna(1).IsEnabled = true;
            //settings.Antennas.GetAntenna(2).IsEnabled = true;
            //settings.Antennas.GetAntenna(3).IsEnabled = true;
            //settings.Antennas.GetAntenna(4).IsEnabled = true;
            //settings.Antennas.GetAntenna(1).TxPowerInDbm = 32.5;
            //settings.Antennas.GetAntenna(2).TxPowerInDbm = 32.5;
            //settings.Antennas.GetAntenna(3).TxPowerInDbm = 32.5;
            //settings.Antennas.GetAntenna(4).TxPowerInDbm = 32.5;
        }
    }
}

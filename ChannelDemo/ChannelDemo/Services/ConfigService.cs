﻿using ChannelDemo.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelDemo.Services
{
    public static class ConfigService
    {
        static ConfigService()
        {
            IsExist();
        }

        private static readonly string channelUrl = AppDomain.CurrentDomain.BaseDirectory + "config\\channel.json";
        private static readonly string readerUrl = AppDomain.CurrentDomain.BaseDirectory + "config\\reader.json";

        public static ChannelSetting ReadChannelSetting()
        {
            var json = File.ReadAllText(channelUrl);

            if (string.IsNullOrEmpty(json)) return new ChannelSetting();

            return JsonConvert.DeserializeObject<ChannelSetting>(json);
        }

        public static ReaderSetting ReadReaderSetting()
        {
            var json = File.ReadAllText(readerUrl);

            if (string.IsNullOrEmpty(json)) return new ReaderSetting();

            return JsonConvert.DeserializeObject<ReaderSetting>(json);
        }

        public static void Save(ChannelSetting setting)
        {
            var json = JsonConvert.SerializeObject(setting);
            File.WriteAllText(channelUrl, json);
        }

        public static void Save(ReaderSetting setting)
        {
            var json = JsonConvert.SerializeObject(setting);
            File.WriteAllText(readerUrl, json);
        }

        private static void IsExist()
        {
            if (!File.Exists(channelUrl))
                File.Create(channelUrl).Close();

            if (!File.Exists(readerUrl))
                File.Create(readerUrl).Close();
        }
    }
}

﻿using ChannelApp.Models;
using System.Collections.ObjectModel;

namespace ChannelDemo.Services
{
    public enum ReaderType
    {
        R420,
        R710,
    }

    public interface IReader
    {
        bool IsOpen { get; }

        ObservableCollection<TagInfo> Tags { get; set; }

        void Connect();

        void Start(bool IsReset = true);

        void Stop();

        void Close();

        void SetParameter(object parameter);
    }
}

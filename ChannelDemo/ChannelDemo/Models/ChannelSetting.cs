﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelDemo.Models
{
    /// <summary>
    /// 通道设置
    /// </summary>
    public class ChannelSetting : BindableBase
    {
        private string name;
        private int baud;
        private int dataBit;
        private int stopBit;
        private int checkBit;

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged(); }
        }

        public int Baud
        {
            get { return baud; }
            set { baud = value; RaisePropertyChanged(); }
        }

        public int DataBit
        {
            get { return dataBit; }
            set { dataBit = value; RaisePropertyChanged(); }
        }

        public int StopBit
        {
            get { return stopBit; }
            set { stopBit = value; RaisePropertyChanged(); }
        }

        public int CheckBit
        {
            get { return checkBit; }
            set { checkBit = value; RaisePropertyChanged(); }
        }
    }
}

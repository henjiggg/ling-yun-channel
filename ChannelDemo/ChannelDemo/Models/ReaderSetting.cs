﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelDemo.Models
{
    public class ReaderSetting : BindableBase
    {
        private string name;
        private double power;
        private int qValue;
        private int session;

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged(); }
        }
         
        public double Power
        {
            get { return power; }
            set { power = value; RaisePropertyChanged(); }
        }

        public int QValue
        {
            get { return qValue; }
            set { qValue = value; RaisePropertyChanged(); }
        }

        public int Session
        {
            get { return session; }
            set { session = value; RaisePropertyChanged(); }
        } 
    }
}

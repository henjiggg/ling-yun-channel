﻿using ChannelApp.Services;
using Prism.Regions;
using System.Threading.Tasks;

namespace ChannelApp.ViewModels
{
    public class AutoViewModel : NavigationViewModel
    {
        public AutoViewModel(Reader420Service reader, SerialPort232Service port232Service)
        {
            Reader = reader;
            Port232Service = port232Service;
        }

        public Reader420Service Reader { get; }

        public SerialPort232Service Port232Service { get; }

        public override void OnNavigatedFrom(NavigationContext navigationContext)
        {
            base.OnNavigatedFrom(navigationContext);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
        }

        private async Task StartAsync()
        {
            while (true)
            {
                var msg = Port232Service.Read();

                if (msg[40] == '1')
                {
                    Reader.Start();
                }
            }
        }
    }
}

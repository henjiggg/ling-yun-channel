﻿using ChannelDemo.Models;
using ChannelDemo.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelApp.ViewModels
{
    internal class SettingViewModel : NavigationViewModel
    {
        public SettingViewModel()
        {
            Model = new ChannelSetting();
            Reader = new ReaderSetting();

            SaveCommand = new DelegateCommand(Save);
            SaveReaderCommand = new DelegateCommand(SaveReader);
        }

        private ChannelSetting model;

        public ChannelSetting Model
        {
            get { return model; }
            set { model = value; RaisePropertyChanged(); }
        }

        private ReaderSetting reader;

        public ReaderSetting Reader
        {
            get { return reader; }
            set { reader = value; RaisePropertyChanged(); }
        }

        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand SaveReaderCommand { get; set; }

        private void SaveReader()
        {
            ConfigService.Save(Reader);
        }

        private void Save()
        {
            ConfigService.Save(Model);
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            Reader = ConfigService.ReadReaderSetting();
            Model = ConfigService.ReadChannelSetting();
            base.OnNavigatedTo(navigationContext);
        }
    }
}

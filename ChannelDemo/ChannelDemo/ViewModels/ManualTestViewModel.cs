﻿using ChannelApp.Models;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelApp.ViewModels
{
    internal class ManualTestViewModel : NavigationViewModel
    {
        public ManualTestViewModel()
        {
            MenuItems = new ObservableCollection<MenuItem>();
            DisplayMenuItems = new ObservableCollection<DisplayMenuItem>();
        }

        private ObservableCollection<MenuItem> menuItems;

        public ObservableCollection<MenuItem> MenuItems
        {
            get { return menuItems; }
            set { menuItems = value; RaisePropertyChanged(); }
        }

        private ObservableCollection<DisplayMenuItem> displayMenuItems;

        public ObservableCollection<DisplayMenuItem> DisplayMenuItems
        {
            get { return displayMenuItems; }
            set { displayMenuItems = value; RaisePropertyChanged(); }
        }


        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            InitItems();
            base.OnNavigatedTo(navigationContext);
        }

        private void InitItems()
        {
            MenuItems.Clear();
            MenuItems.Add(new MenuItem() { Name = "卷帘上升", Key = "M50", });
            MenuItems.Add(new MenuItem() { Name = "卷帘下降", Key = "M51", });
            MenuItems.Add(new MenuItem() { Name = "A段前进", Key = "M52", });
            MenuItems.Add(new MenuItem() { Name = "A段停止", Key = "M53", });
            MenuItems.Add(new MenuItem() { Name = "A段后退", Key = "M54", });
            MenuItems.Add(new MenuItem() { Name = "B段前进", Key = "M55", });
            MenuItems.Add(new MenuItem() { Name = "B段停止", Key = "M56", });
            MenuItems.Add(new MenuItem() { Name = "B段后退", Key = "M57", });
            MenuItems.Add(new MenuItem() { Name = "C段前进", Key = "M58", });
            MenuItems.Add(new MenuItem() { Name = "C段停止", Key = "M59", });
            MenuItems.Add(new MenuItem() { Name = "C段后退", Key = "M60", });
            MenuItems.Add(new MenuItem() { Name = "D顶升上升", Key = "M61", });
            MenuItems.Add(new MenuItem() { Name = "D顶升下降", Key = "M62", });
            MenuItems.Add(new MenuItem() { Name = "F剔除启动", Key = "M63", });
            MenuItems.Add(new MenuItem() { Name = "G分拣动力前进", Key = "M64", });
            MenuItems.Add(new MenuItem() { Name = "G分拣动力后退", Key = "M65", });
            MenuItems.Add(new MenuItem() { Name = "H回流段动力", Key = "M66", });

            DisplayMenuItems.Clear();
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "B/1-光电", Key = "M0001" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "B/2-光电", Key = "M0002" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "B/3-光电", Key = "M0003" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "B/4-光电", Key = "M0004" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "A/1-光电", Key = "M0005" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "A/2-光电", Key = "M0006" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "C/1-号光电", Key = "M0007" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "前门-行程上限", Key = "M0033" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "前门-行程下限", Key = "M0034" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "后门-行程上限", Key = "M0035" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "后门-行程下限", Key = "M0036" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "开门状态", Key = "M0037" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "屏蔽状态", Key = "M0038" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "自动化模式", Key = "M0039" });
            DisplayMenuItems.Add(new DisplayMenuItem() { Name = "卷帘故障", Key = "M0040" }); 
        }
    }
}

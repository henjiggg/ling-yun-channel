﻿using ChannelApp.Services;
using ChannelDemo.Services;
using Prism.Commands;
using Prism.Ioc;

namespace ChannelApp.ViewModels
{
    public class TestModeViewModel : NavigationViewModel
    {
        private readonly SerialPort232Service channelService;
        public IReader Reader { get; set; }

        public TestModeViewModel(SerialPort232Service channelService)
        {
            Reader = ContainerLocator.Container.Resolve<IReader>(ReaderType.R710.ToString());
            this.channelService = channelService;
            OpenReaderCommand = new DelegateCommand(OpenReader);
            CloseReaderCommand = new DelegateCommand(CloseReader);
        }

        public DelegateCommand OpenReaderCommand { get; private set; }
        public DelegateCommand CloseReaderCommand { get; private set; }

        private void OpenReader()
        {
            Reader.Connect();
            Reader.Start();
        }

        private void CloseReader()
        {
            Reader.Close();
        }
    }
}

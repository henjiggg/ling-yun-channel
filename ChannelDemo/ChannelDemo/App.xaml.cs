﻿using ChannelApp.Services;
using ChannelApp.ViewModels;
using ChannelApp.Views;
using ChannelDemo.Services;
using Prism.DryIoc;
using Prism.Ioc;
using System.Windows;

namespace ChannelDemo
{
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainView>();
        }

        protected override void RegisterTypes(IContainerRegistry services)
        {
            services.RegisterSingleton<IReader, Reader420Service>(ReaderType.R420.ToString());
            services.RegisterSingleton<IReader, Reader710Service>(ReaderType.R710.ToString());
            services.RegisterSingleton<SerialPort232Service>();
            services.RegisterForNavigation<MainView, MainViewModel>();
            services.RegisterForNavigation<SettingView, SettingViewModel>();
            services.RegisterForNavigation<AutoView, AutoViewModel>();
            services.RegisterForNavigation<TestModeView, TestModeViewModel>();
            services.RegisterForNavigation<ManualTestView, ManualTestViewModel>();
        }
    }
}

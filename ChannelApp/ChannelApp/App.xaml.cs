﻿using ChannelApp.Services;
using ChannelApp.ViewModels;
using ChannelApp.Views;
using Prism.DryIoc;
using Prism.Ioc;
using System.Windows;

namespace ChannelApp
{
    public partial class App : PrismApplication
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainView>();
        }

        protected override void RegisterTypes(IContainerRegistry services)
        {
            services.RegisterSingleton<Reader420Service>();
            services.RegisterSingleton<SerialPort232Service>();
            services.RegisterForNavigation<MainView, MainViewModel>();
            services.RegisterForNavigation<SettingView, SettingViewModel>();
            services.RegisterForNavigation<AutoView, AutoViewModel>();
            services.RegisterForNavigation<TestModeView, TestModeViewModel>();
            services.RegisterForNavigation<ManualTestView, ManualTestViewModel>();
        }
    }
}

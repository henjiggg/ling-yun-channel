﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelApp.Models
{
    public class MenuItem
    {
        //名称
        public string Name { get; set; }

        //指令代码
        public string Key { get; set; }

        //报文
        public string Value { get; set; }
    }

    public class DisplayMenuItem
    {
        public string Name { get; set; }

        public string Key { get; set; }

        public string Desc { get; set; }
    }
}

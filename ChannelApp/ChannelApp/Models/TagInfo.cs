﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelApp.Models
{
    public class TagInfo  
    {
        public string TID { get; set; }

        public string EPC { get; set; }

        public DateTime Date { get; set; }
    }
}

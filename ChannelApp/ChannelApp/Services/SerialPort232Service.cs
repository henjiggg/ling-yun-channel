﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelApp.Services
{
    /// <summary>
    /// 232通讯服务
    /// </summary>
    public class SerialPort232Service
    {
        public bool IsOpen => serialPort.IsOpen;

        public SerialPort serialPort = new SerialPort();

        public void Open()
        {
            if (!serialPort.IsOpen)
            {
                serialPort.Open();
            }
        }

        public void Close()
        {
            if (serialPort.IsOpen)
                serialPort.Close();
        }

        public string Read()
        {
            string str = "";
            byte[] bytesM = HexStringToByteArray("02 30 30 31 30 30 31 34 03 35 39");
            serialPort.Write(bytesM, 0, bytesM.Length);
            Thread.Sleep(200);

            byte[] recvBytes = new byte[serialPort.BytesToRead];
            int bytes = serialPort.Read(recvBytes, 0, recvBytes.Length);
            string x16 = Encoding.ASCII.GetString(recvBytes, 1, bytes - 4);// 1 -4

            for (int i = 0; i < x16.Length; i += 2)
            {
                str = Convert.ToString(Convert.ToInt32(x16[i].ToString(), 16), 2).PadLeft(4, '0') + Convert.ToString(Convert.ToInt32(x16[i + 1].ToString(), 16), 2).PadLeft(4, '0') + str;
            }
            str = new string(str.Reverse().ToArray());
            return str;
        }

        public void Write(string cmd, bool isReset = true)
        {
            string address = Convert.ToInt32(cmd.Substring(1, 4)).ToString("X2");
            string hexStr = "";
            int sum = 0;//求和校验的信息
            for (int i = 0; i < address.Length; i++)
            {
                hexStr += Convert.ToInt32(address[i]).ToString("X2") + " ";
                sum += Convert.ToInt32(Convert.ToInt32(address[i]).ToString("X2"), 16);
            }
            if (isReset) sum += 162;
            else sum += 163;
            string s16 = sum.ToString("X2");
            string str1 = Convert.ToInt32(s16[s16.Length - 2]).ToString("x2");
            string str2 = Convert.ToInt32(s16[s16.Length - 1]).ToString("x2");

            string command = "";
            if (isReset)
                command = "02 37 " + hexStr + "30 38 03 " + str1 + " " + str2;
            else
                command = "02 38 " + hexStr + "30 38 03 " + str1 + " " + str2;
            byte[] bytes = HexStringToByteArray(command);
            serialPort.Write(bytes, 0, bytes.Length);

            if (isReset)
                Write(cmd, false);
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
            {
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            }
            return buffer;
        }
    }
}

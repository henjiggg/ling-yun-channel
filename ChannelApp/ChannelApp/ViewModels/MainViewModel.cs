﻿using ChannelApp.Services;
using Prism.Commands;
using Prism.Regions;
using System;

namespace ChannelApp.ViewModels
{
    public class MainViewModel : NavigationViewModel
    {
        private readonly IRegionManager region;

        public MainViewModel(IRegionManager region)
        {
            NavigateCommand = new DelegateCommand<string>(Navigate);
            this.region = region;
        }

        private void Navigate(string pageName)
        {
            region.Regions["MainRegion"].RequestNavigate(pageName);
        }

        public DelegateCommand<string> NavigateCommand { get; private set; }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        { 
            base.OnNavigatedTo(navigationContext);
        }
    }
}

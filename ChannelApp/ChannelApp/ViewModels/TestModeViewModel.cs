﻿using ChannelApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelApp.ViewModels
{
    public class TestModeViewModel : NavigationViewModel
    {
        public TestModeViewModel(Reader420Service reader, SerialPort232Service port232Service)
        {
            Reader = reader;
            Port232Service = port232Service;
        }

        public Reader420Service Reader { get; }

        public SerialPort232Service Port232Service { get; }
    }
}
